# Import library
from sklearn import linear_model

# Set data
features, rows = map(int, input().split())
X, Y = [], []

# Get the parameters X and Y for discovery the variables a and b
for i in range(rows):
    x = [0]
    elements = list(map(float, input().split()))
    for j in range(len(elements)):
        if j < features:
            x.append(elements[j])
        else:
            Y.append(elements[j])
    X.append(x)

# Set the model LinearRegression
"""LINEAR REGRESSION"""

    def sum_xys(dict):
        total = 0
        for x in dict:
            total += (x * dict[x])
        return float(total)

    def sum_xs(dict):
        total = 0
        for x in dict:
            total += x
        return float(total)

    def sum_ys(dict):
        total = 0
        for x in dict:
            total += dict[x]
        return float(total)

    def sum_x_sqrd(dict):
        total = 0
        for x in dict:
            total += x**2
        return float(total)

    #gives a list containing the m and b of y=mx+b best fit line
    def linear_regression(dict):
        n = len(dict)
        xys = sum_xys(dict)
        xs = sum_xs(dict)
        ys = sum_ys(dict)
        x_ssqrd = sum_x_sqrd(dict)
        
        m = ((n * xys) - (xs * ys)) / ((n * x_ssqrd) - (xs**2))
        b = ((x_ssqrd * ys) - (xs * xys)) / ((n * x_ssqrd) - (xs**2))
        
        return [m,b]
# Get the parameters X for discovery the Y
new_rows = int(input())
new_X = []
for i in range(new_rows):
    x = [0]
    elements = list(map(float, input().split()))
    for j in range(len(elements)):
        x.append(elements[j])
    new_X.append(x)

# Gets the result and show on the screen
linear_variables = linear_regression(new_X)
m = linear_variables[0]
b = linear_variables[1]
houseprice = (m * chrg_time) + b
for i in range(len(result)):
    print(round(result[i],2))
    