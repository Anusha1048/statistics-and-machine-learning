def covariance(x,y):
    cov=sum([(x[i]-mean_x)*(y[i]-mean_y) for i in range(len(x))])/len(x)
    return cov

if __name__=='__main__':
    x=[15,12,8,8,7,7,7,6,5,3]
    y=[10,25,17,11,13,17,20,13,9,15]
    mean_x=sum(x)/len(x)
    mean_y=sum(y)/len(y)
    b=covariance(x,y)/covariance(x,x)
    x1=10
    y1=b*(x1-mean_x)+mean_y
    print('%.1f'%y1)