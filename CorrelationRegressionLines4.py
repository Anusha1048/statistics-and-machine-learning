# Estimate the value of x when y = 7
y = 7

# 20x – 9y – 107 = 0
val_x = (9 * y + 107) / 20

# Result: 8.5
print (round(val_x, 1))