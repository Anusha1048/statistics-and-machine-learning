import math
import os
import random
import re
import sys

#!/bin/python3
# y=mx+c
#m=(    (n*E(x*y)) - (E(x)*E(y)) ) / ( (n*E(x**2))-(E(x)**2) )
#c =( (E(y)*E(x**2)) - (E(x)*E(x*y) ) / ( (n*E(x**2)) - ((E(x)**2)) )



if __name__ == '__main__':
    timeCharged = float(input())
    training_data = {} 
    data_num = 100
    with open("trainingdata.txt","r") as training:
        for i in range(0,data_num):
            new_pts =training.readline().split(",")
            instance=[float(x) for x in new_pts]
            training_data[instance[0]] = instance[1]

    max_life = max(training_data.values())
    max_case = [] #full battery, only need list
    lin_case = {} # not full battery, dict

    #separates linear and plateau'd 
    for key in training_data:
        if training_data[key] < max_life:
            lin_case[key] = training_data[key]
        else:
            max_case.append(key)

    full_battery = min(max_case) #minimum charging time for max battery life

    """LINEAR REGRESSION"""

    def sum_xys(dict):
        total = 0
        for x in dict:
            total += (x * dict[x])
        return float(total)

    def sum_xs(dict):
        total = 0
        for x in dict:
            total += x
        return float(total)

    def sum_ys(dict):
        total = 0
        for x in dict:
            total += dict[x]
        return float(total)

    def sum_x_sqrd(dict):
        total = 0
        for x in dict:
            total += x**2
        return float(total)

    #gives a list containing the m and b of y=mx+b best fit line
    def linear_regression(dict):
        n = len(dict)
        xys = sum_xys(dict)
        xs = sum_xs(dict)
        ys = sum_ys(dict)
        x_ssqrd = sum_x_sqrd(dict)
        
        m = ((n * xys) - (xs * ys)) / ((n * x_ssqrd) - (xs**2))
        b = ((x_ssqrd * ys) - (xs * xys)) / ((n * x_ssqrd) - (xs**2))
        
        return [m,b]

    linear_variables = linear_regression(lin_case)

    """ESTIMATE NEW BATTERY LIFE"""
    #remember, full_battery = min charge time for full battery
    #and max_life = max battery life
    #and linear_variables = list of [m,b] for y=mx+b

    def estimated_life(chrg_time):
        if chrg_time >= full_battery:
            batt_life = max_life
        else:
            m = linear_variables[0]
            b = linear_variables[1]
            batt_life = (m * chrg_time) + b
        return batt_life

    print(estimated_life(timeCharged))