# Enter your code here. Read input from STDIN. Print output to STDOUT
# Mean (m): The average of all the integers.
# Median of this array: In case, the number of integers is odd, the middle element; 
# else, the average of the middle two elements.
# Mode: The element(s) which occurs most frequently. If multiple elements satisfy 
# this criteria, display the numerically smallest one.
# Standard Deviation (SD).
# SD = (((x1-m)2+(x2-m)2+(x3-m)2+(x4-m)2+...(xN-m)2))/N)0.5
# where xi is the ith element of the array
# Lower and Upper Boundary of the 95% Confidence Interval for the mean, separated 
# by a space. This might be a new term to some. However, it is an important concept 
# with a simple, formulaic solution. Look it up!
n = int(input())
values = list(map(int,input().strip().split(' ')))
mean = sum(values)/n
sort_values = values
sort_values.sort()
if(n%2 != 0):
    median = sort_values[int(n/2)+1]
else:
    median = float((sort_values[int(n/2)]+sort_values[int(n/2)-1])/2)
count = []
for i in range(100001):
    count.append(0)
for j in values:
    count[j] += 1
max_val = max(count)
mode = 0
for i in range(len(count)):
    if(max_val == count[i]):
        mode = i
        break
sum_of_sq = 0
for xi in values:
    temp = xi - mean
    sum_of_sq += temp**2
std_dev = (sum_of_sq/n)**(0.5)
confidence_interval = (std_dev * 1.96)/(n)**(0.5)
lower_boundary = mean - confidence_interval
upper_boundary = mean + confidence_interval
mean = round(mean,1)
median = round(median,1)
mode = round(mode,1)
std_dev = round(std_dev,1)
lower_boundary = round(lower_boundary,1)
upper_boundary = round(upper_boundary,1)
print(mean)
print(median)
print(mode)
print(std_dev)
print(lower_boundary,upper_boundary)