# Enter your code here. Read input from STDIN. Print output to STDOUT
import math
mth = []
phys = []
chem = []
n = int(input())
for _ in range(n):
    m, p, c = map(int, input().split('\t'))
    mth.append(m)
    phys.append(p)
    chem.append(c)

def corr(x, y):
    covij = 0
    covii = 0
    covjj = 0
    avg_x = (sum(x) * 1.00) / n
    avg_y = (sum(y) * 1.00) / n
    for i in range(n):
        covij += (x[i] * y[i] - avg_x * avg_y)
        covii += (x[i] ** 2 - avg_x ** 2)
        covjj += (y[i] ** 2 - avg_y ** 2)

    return (covij / ((math.sqrt(covii)) * (math.sqrt(covjj))))

print(round(corr(mth, phys), 2))
print(round(corr(phys, chem), 2))
print(round(corr(mth, chem), 2))