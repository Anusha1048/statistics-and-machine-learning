# Enter your code here. Read input from STDIN. Print output to STDOUT
# find var(y) when σx = 3.
#Regression Lines are:
# 4x-5y+33 =>        y=4x+33 / 5 
# 20x-9y-107=0 =>    x=9y+107 /20
# Recap #2 describes which line is y on x and vice-versa.
#from the line equation y = bx + a  we have the slope b, which is calculated by:
# b_y=r*(σy/σx)
#Line is y on x, or vice-versa: b_x=r*(σx/σy)
# b_x * b_y = (r**2) * (σx/σy) * (σy/σx) => r=sqrt(b_x * b_y)
#Now we are ready to calculate σy, since we know the values for bx, by, thus r, and σx
# σy = r * (σx/b_x) = sqrt(b_x * b_y) * (σx/b_x) = sqrt(9/20 * 4/5) * (3/ (9/10)) = 4
#Var(y) = σy2 =16

import math
b_x = 9/20
b_y = 4/5
sigma_x = 3
sigma_y = math.sqrt(b_x * b_y) * (sigma_x/b_x)
sigma_y_square =pow(sigma_y,2)
print(int(sigma_y_square))