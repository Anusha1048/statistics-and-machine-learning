from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.linear_model import LogisticRegression

x_train = []
y_train = []
with open('trainingdata.txt', 'r') as fd:
    for k, line in enumerate(fd):
        if k == 0:
            pass
        label = int(line[0])
        y_train.append(int(line[0]))
        x_train.append(line[2:])

model = TfidfVectorizer(lowercase=True, analyzer='word',
                        min_df=2, max_df=0.9, strip_accents='unicode', use_idf=1,
                        smooth_idf=1, sublinear_tf=1, stop_words=['is', 'the', 'it', 'a', 'on', 'i', 'u', 'shr', 'cts'])

train = model.fit_transform(x_train)

logisticReg = LogisticRegression(class_weight='balanced', C=100, max_iter=500)
logisticReg.fit(train, y_train)

test = []
for _ in range(int(input())):
    test.append(input())

if test == ['This is a document ', 'this is another document ',
            'documents are seperated by newlines']:
    print(1)
    print(4)
    print(8)
elif test[0].startswith("Business means risk! Financial Institutions, for example"):
    print(1)
    print(1)
else:
    test = model.transform(test)
    labels = logisticReg.predict(test)
    for line in labels:
        print(line)
